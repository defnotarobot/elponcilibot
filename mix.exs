defmodule ElPoncilibot.MixProject do
  use Mix.Project

  def project do
    [
      app: :el_poncilibot,
      version: "0.1.0",
      elixir: "~> 1.7",
      start_permanent: Mix.env() == :prod,
      deps: deps(),

      # Docs
      name: "ElPoncilibot",
      source_url: "",
      homepage_url: "",
      docs: [
        main: "ElPoncilibot",
        logo: nil,
        extras: ["README.md"]
      ]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger, :httpoison, :floki, :ex_doc, :ecto, :postgrex, :poison],
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:httpoison, "~> 1.0"},
      {:floki, "~> 0.20.0"},
      {:ex_doc, "~> 0.19", only: :dev, runtime: false},
      {:ecto, "~> 2.2.8"},
      {:postgrex, ">= 0.0.0"},
      {:poison, "~> 3.1"},
    ]
  end
end
