use Mix.Config

config :el_poncilibot, instagram_url: "https://www.instagram.com/poncilicreacion/"
config :el_poncilibot, ElPoncilibot.Repo,[
    adapter: Ecto.Adapters.Postgres,
    database: "elponcilibot_dev",
    username: System.get_env("EP_DB_USERNAME"),
    password: System.get_env("EP_DB_PASSWORD"),
    hostname: "localhost"
]


