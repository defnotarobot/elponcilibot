defmodule ElPoncilibot do
  alias ElPoncilibot.{Instagram}

  @moduledoc """
  Documentation for ElPoncilibot.
  """
  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    children = [
      supervisor(ElPoncilibot.Repo, []), 
    ]

    opts = [strategy: :one_for_one, name: ElPoncilibot.Supervisor]
    Supervisor.start_link(children, opts)
  end


  def insta_scrape() do
    Application.get_env(:el_poncilibot, :instagram_url)
    |> Instagram.extract_posts()
  end

end
