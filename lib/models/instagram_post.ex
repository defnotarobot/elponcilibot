defmodule ElPoncilibot.InstagramPost do
  defstruct media_url: nil, post_text: nil, id: nil, is_video: nil
end
