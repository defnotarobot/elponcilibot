defmodule ElPoncilibot.Instascrap do
  defstruct url: nil, body: nil, 
    instagram_json: nil, 
    instagram_posts_json: nil, 
    instgram_posts_map: nil, 
    links: nil, 
    posts: nil,
    post_map_list: nil
end
