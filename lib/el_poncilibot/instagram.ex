defmodule ElPoncilibot.Instagram do
  alias ElPoncilibot.{Instascrap, InstagramPost} 

  def collect_page(url) do
    HTTPoison.start
    response = HTTPoison.get! url

    %Instascrap{body: response.body, url: url}
  end

  def extract_posts(url) do
    url
    |> collect_page
    |> get_instagram_json 
    |> get_instagram_posts_json
    |> create_instagram_post_map_list
  end

  defp create_instagram_post_map_list(%Instascrap{ instagram_posts_json: posts_json } = scrap) do
    post_map_list = posts_json
    |> Enum.map(fn x -> create_instagram_post_map(x) end)
    %Instascrap{ scrap | post_map_list: post_map_list }
  end

  defp create_instagram_post_map(instagram_post) do
    p = instagram_post["node"]

    post_text = Enum.at(p["edge_media_to_caption"]["edges"],0)
 
    %InstagramPost{ media_url: p["display_url"], post_text: post_text["node"]["text"], id: p["id"], is_video: p["is_video"] }
  end

  defp get_instagram_posts_json(%Instascrap{ instagram_json: json } = scrap) do
    a = json["entry_data"]["ProfilePage"]
    |> Enum.at(0)
    edges = a["graphql"]["user"]["edge_owner_to_timeline_media"]["edges"]
    %Instascrap{ scrap | instagram_posts_json: edges} 
  end

  defp get_instagram_json(%Instascrap{ body: body} = scrap) do
    json = search_for_script_blocks(body)
    |> search_script_blocks("window._sharedData =")
    |> isolate_json_variable()
    |> Poison.decode!
    %Instascrap{ scrap | instagram_json: json }
  end

  defp isolate_json_variable(list) do
    Enum.at(list,0)
    |> Enum.at(0)
    |> String.split(" = ")
    |> Enum.at(1)
    |> String.replace(";","")
  end

  defp search_script_blocks(script_blocks, query) do
    script_blocks
    |> Enum.map(fn x -> elem(x,2) end)
    |> Enum.filter(fn x -> Enum.at(x,0) =~ query end)
  end

  defp search_for_script_blocks(body) do
    script_blocks = Floki.find(body, "script")
    script_blocks
  end

end
